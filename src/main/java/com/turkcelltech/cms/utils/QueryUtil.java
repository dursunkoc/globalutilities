/**
 * 
 */
package com.turkcelltech.cms.utils;

import static com.turkcelltech.cms.utils.ArrayUtil.nullSafeList;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Joiner;

/**
 * @author TTDKOC
 * 
 */
public class QueryUtil {
	private static final Joiner arrayJoiner = Joiner.on(", ");

	/**
	 * @param list
	 * @param mainEntity
	 * @param params
	 * @return
	 */
	public static <T> T oneAndOnlyOne(List<T> list, String mainEntity,
			Object... params) {
		if (list == null || list.isEmpty()) {
			throw new RuntimeException("No such " + mainEntity + "("
					+ arrayJoiner.join(params) + ") found. ");
		} else if (list.size() > 1) {
			throw new RuntimeException("Too many " + mainEntity + "("
					+ arrayJoiner.join(params) + ") found. ");
		} else {
			return list.get(0);
		}
	}

	/**
	 * @param t
	 * @return
	 */
	public static <T> boolean oneAndOnlyOneNotNull(T... t) {
		int nrOfNotNull = 0;
		for (T o : t) {
			if (o != null) {
				nrOfNotNull++;
			}
			if (nrOfNotNull > 1) {
				return false;
			}
		}
		return nrOfNotNull == 1;
	}

	public static <T> T onlyOneOrDefault(List<T> objects, T defaultObject,
			Object... params) {
		if (objects == null || objects.isEmpty()) {
			return defaultObject;
		} else if (objects.size() > 1) {
			throw new RuntimeException("Too many records for parameters("
					+ Arrays.asList(params) + ")");
		}
		return objects.get(0);
	}

	/**
	 * @param ts
	 * @return
	 */
	public static <T> T onlyOneOrNull(List<T> ts) {
		if (nullSafeList(ts).isEmpty()) {
			return null;
		}
		if (ts.size() != 1) {
			throw new RuntimeException("Expected one record but got multiple!");
		}
		return ts.get(0);
	}

}
