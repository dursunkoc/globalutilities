/**
 * 
 */
package com.turkcelltech.cms.utils;

import static com.turkcelltech.cms.utils.BooleanUtil.Not;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
/**
 * @author TTDKOC
 * 
 */
public final class ArrayUtil {
	private ArrayUtil() {

	}

	public static <T> boolean nullOrEmptyOrAllNull(T[] a) {
		if (a == null)
			return true;

		if (a.length == 0)
			return true;

		for (T t : a) {
			if (t != null)
				return false;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] dropNullItems(final T[] a, Class<T> clazz) {
		if (a == null || a.length == 0) {
			return a;
		}
		ArrayList<T> l = new ArrayList<T>(a.length);
		for (T t : a) {
			if (t != null) {
				l.add(t);
			}
		}
		return l.toArray((T[]) Array.newInstance(clazz, l.size()));
	}

	/**
	 * @param l
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> nullSafeList(List<T> l) {
		return (l == null ? (List<T>) Collections.emptyList() : l);
	}

	/**
	 * @param m
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> nullSafeMap(Map<K, V> m) {
		return (m == null ? (Map<K, V>) Collections.emptyMap() : m);
	}

	/**
	 * @param ts
	 * @return
	 */
	public static <T> boolean NotNull(T... ts) {
		return notNull(ts);
	}

	/**
	 * @param ts
	 * @return
	 */
	public static <T> boolean notNull(T... ts) {
		for (T t : ts) {
			if (Null(t)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param ts
	 * @return
	 */
	public static <T> boolean Null(T ts) {
		return ts == null;
	}

	public static <T> boolean in(T refVal, T[] lookup) {
		if (Null(refVal) || Null(lookup)) {
			return false;
		}
		for (T t : lookup) {
			if (Not(Null(t)) && refVal.equals(t)) {
				return true;
			}
		}
		return false;
	}

	public static <T> boolean in(T refVal, List<T> lookup) {
		if (Null(refVal) || Null(lookup)) {
			return false;
		}
		for (T t : lookup) {
			if (Not(Null(t)) && refVal.equals(t)) {
				return true;
			}
		}
		return false;
	}
}
