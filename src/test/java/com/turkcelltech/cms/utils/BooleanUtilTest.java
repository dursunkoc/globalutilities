/**
 * 
 */
package com.turkcelltech.cms.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author TTDKOC
 * 
 */
public class BooleanUtilTest {
	@Test
	public void testNot() {
		boolean not = BooleanUtil.Not(false);
		Assert.assertTrue(not);
		not = BooleanUtil.Not(true);
		Assert.assertFalse(not);
	}

	@Test
	public void testNot1() {
		boolean not = BooleanUtil.not(false);
		Assert.assertTrue(not);
		not = BooleanUtil.not(true);
		Assert.assertFalse(not);
	}

}
