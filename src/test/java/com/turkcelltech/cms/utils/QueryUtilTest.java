/**
 * 
 */
package com.turkcelltech.cms.utils;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author TTDKOC
 * 
 */
public class QueryUtilTest {

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#oneAndOnlyOne(java.util.List, java.lang.String, java.lang.Object[])}
	 * .
	 */
	@Test(expected = RuntimeException.class)
	public void testOneAndOnlyOneShouldThrowExceptionOnMultipleResults() {
		QueryUtil.oneAndOnlyOne(Arrays.asList(1, 2, 3, 4, 5), "Main Entity", 1,
				2);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#oneAndOnlyOne(java.util.List, java.lang.String, java.lang.Object[])}
	 * .
	 */
	@Test(expected = RuntimeException.class)
	public void testOneAndOnlyOneWithEmptyList() {
		QueryUtil.oneAndOnlyOne(Collections.<Integer> emptyList(),
				"Main Entity", 1, 2);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#oneAndOnlyOne(java.util.List, java.lang.String, java.lang.Object[])}
	 * .
	 */
	@Test()
	public void testOneAndOnlyOne() {
		int oneAndOnlyOne = QueryUtil.oneAndOnlyOne(Arrays.asList(1),
				"Main Entity", 1, 2);
		Assert.assertEquals(1, oneAndOnlyOne);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#oneAndOnlyOneNotNull(T[])}.
	 */
	@Test
	public void testOneAndOnlyOneNotNull() {
		boolean r = QueryUtil.oneAndOnlyOneNotNull(1, 2, 3, null, 4);
		Assert.assertFalse(r);
		r = QueryUtil.oneAndOnlyOneNotNull(null, null);
		Assert.assertFalse(r);
		r = QueryUtil.oneAndOnlyOneNotNull(1, 2, 3, 4);
		Assert.assertFalse(r);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#onlyOneOrDefault(java.util.List, java.lang.Object, java.lang.Object[])}
	 * .
	 */
	@Test(expected = RuntimeException.class)
	public void testOnlyOneOrDefaultShouldThrowExceptionOnMultipleResults() {
		QueryUtil.onlyOneOrDefault(Arrays.asList(1, 2, 3, 4, 5), 9, 1, 2, 3);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#onlyOneOrDefault(java.util.List, java.lang.Object, java.lang.Object[])}
	 * .
	 */
	@Test
	public void testOnlyOneOrDefault() {
		int onlyOneOrDefault = QueryUtil.onlyOneOrDefault(Arrays.asList(1), 9,
				1, 2, 3);
		Assert.assertEquals(1, onlyOneOrDefault);

		onlyOneOrDefault = QueryUtil.onlyOneOrDefault(
				Collections.<Integer> emptyList(), 9, 1, 2, 3);
		Assert.assertEquals(9, onlyOneOrDefault);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.utils.QueryUtil#onlyOneOrNull(java.util.List)}
	 * .
	 */
	@Test
	public void testOnlyOneOrNull() {
		Integer onlyOneOrNull = QueryUtil.onlyOneOrNull(Arrays.asList(1));
		Assert.assertEquals(new Integer(1), onlyOneOrNull);

		try {
			QueryUtil.onlyOneOrNull(Arrays.asList(1, 2, 3));
		} catch (RuntimeException e) {
			Assert.assertEquals("Expected one record but got multiple!",
					e.getMessage());
		}
	}

}
