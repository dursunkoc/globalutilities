/**
 * 
 */
package com.turkcelltech.cms.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author TTDKOC
 * 
 */
public class ArrayUtilTest {

	@Test
	public void testNullOrEmptyOrAllNull() {
		boolean r = ArrayUtil.nullOrEmptyOrAllNull(null);
		Assert.assertTrue("Null value should return true", r);

		r = ArrayUtil.nullOrEmptyOrAllNull(new Object[] {});
		Assert.assertTrue("Empty array should return true", r);

		r = ArrayUtil.nullOrEmptyOrAllNull(new Object[] { null, null });
		Assert.assertTrue(
				"NonEmpty array with null elements should return true", r);

		r = ArrayUtil.nullOrEmptyOrAllNull(new Object[] { new Object(), null });
		Assert.assertFalse(
				"NonEmpty array with not null Elements should return false", r);
	}

	@Test
	public void testDropNullItems() {
		Object[] dropNullItems = ArrayUtil.dropNullItems(new Object[] { new Object(), new Object(),
				null, new Object(), new Object(), null, new Object() },
				Object.class);
		Assert.assertEquals(5, dropNullItems.length);
	}

	@Test
	public void testNullSafeList() {
		List<?> nullSafeList = ArrayUtil.nullSafeList(null);
		Assert.assertNotNull("Null safe list should return not null list",
				nullSafeList);
		Assert.assertTrue("Null safe list should return empty list",
				nullSafeList.isEmpty());
	}

	@Test
	public void testNullSafeMap() {
		Map<?, ?> nullSafeMap = ArrayUtil.nullSafeMap(null);
		Assert.assertNotNull("Null safe map should return not null list",
				nullSafeMap);
		Assert.assertTrue("Null safe map should return empty map",
				nullSafeMap.isEmpty());
	}

	@Test
	public void testNotNull() {
		boolean notNull = ArrayUtil.NotNull(null, null, new Object());
		Assert.assertFalse(notNull);

		notNull = ArrayUtil.NotNull(new Object(), null, null);
		Assert.assertFalse(notNull);
	}

	@Test
	public void testNotNull1() {
		boolean notNull = ArrayUtil.notNull(null, null, new Object());
		Assert.assertNotNull(notNull);

		notNull = ArrayUtil.notNull(new Object(), null, null);
		Assert.assertNotNull(notNull);
	}

	@Test
	public void testNull() {
		boolean null_ = ArrayUtil.Null(null);
		Assert.assertTrue(null_);

		null_ = ArrayUtil.Null(new Object());
		Assert.assertFalse(null_);
	}

	@Test
	public void testInTTArray() {
		boolean r = ArrayUtil.in(1, Arrays.asList(1, 2, 3, 4, 5, 6).toArray());
		Assert.assertTrue(r);

		r = ArrayUtil.in(10, Arrays.asList(1, 2, 3, 4, 5, 6).toArray());
		Assert.assertFalse(r);

	}

	@Test
	public void testInTListOfT() {
		boolean r = ArrayUtil.in(1, Arrays.asList(1, 2, 3, 4, 5, 6));
		Assert.assertTrue(r);

		r = ArrayUtil.in(10, Arrays.asList(1, 2, 3, 4, 5, 6));
		Assert.assertFalse(r);

	}

}
